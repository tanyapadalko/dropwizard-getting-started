import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

@Path("/convert")
@Produces(MediaType.APPLICATION_JSON)
public class ConverResource {

    @GET
    @Timed
    public String convert(@QueryParam("number") Optional<Long> number) {
        return Long.toBinaryString(number.orElse(getMillisFromDate()));
    }

    private long getMillisFromDate() {
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime zdt = now.atZone(ZoneId.systemDefault());
        return zdt.toInstant().toEpochMilli();
    }
}
